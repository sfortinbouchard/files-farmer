#!/usr/bin/env bats

setup() {
   run rm -rf target
   run mkdir target
}

@test ": tdd go" {
  [ 1 -eq 1 ]
}

@test ": can harvest a directory with two files" {
   run mkdir target/field
   run touch target/field/first
   run touch target/field/second

   run cp farmer.sh target/farmer.sh

   run target/farmer.sh target/field target/crops

   grep first target/crops
   grep second target/crops
}

