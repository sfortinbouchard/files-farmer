#!/usr/bin/env bats

setup() {
   run rm -rf target
   run mkdir target
   run cp script-decorator.sh target/script-decorator.sh
}

@test ": can decorate one file" {
   echo "first" > target/crops

   run target/script-decorator.sh target/crops target/sexy-crops

   grep '<script src="first"></script>' target/sexy-crops
}

